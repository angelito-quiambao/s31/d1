
//require express module again
const express = require('express');
const router = express.Router();

const taskController = require('./../controllers/taskControllers')

//CREATE A TASK
router.post('/', async (req, res) => {
	// console.log(req.body)	//object

    try{
      await taskController.createTask(req.body).then(result => res.send(result))
    }
    catch(err){
      res.status(400).json(err.message)
    }
})

//GET ALL TASK
router.get('/', async (req, res) => {
	try{
    await taskController.getAllTasks().then(result => res.send(result))

  }
  catch(err){
    res.status(500).json(err.message)
  }
})

//DELETE A TASK
router.delete('/:taskId/delete', async (req, res) =>{
   //console.log(typeof req.params.taskId) //object

  try{
      await taskController.deleteTask(req.params.taskId).then(result => res.send(result))
  }
  catch(err){
    res.status(500).json(err.message)
  }
})


//UPDATE A TASK
router.put('/:taskId', async (req, res) => {
    // console.log(req.params.taskId)
    const id = req.params.taskId
    // console.log(req.body)
    try{
      await taskController.updateTask(id, req.body).then(result => res.send(result))
    }
    catch(err){
      res.status(500).json(err.message)
    }
   
})

//GET A SPECIFIC TASK

router.get('/:taskId', async (req, res) => {
  //console.log(req.params.taskId);
  try{
    await taskController.getOneTasks(req.params.taskId).then(result => res.send(result))

  }
  catch(err){
    res.status(500).json(err.message)
  }
})

//UPDATE A SPECIFIC TASK TO CHANGE STATUS TO COMPLETE
router.put('/:taskId/complete', async (req, res) => {
    //console.log(req.params.taskId);
    const id = req.params.taskId;
  try{
    await taskController.updateTaskStatus(id, req.body).then(result => res.send(result))
  }
  catch(err){
   res.status(500).json(err.message)
  }
 
})

module.exports = router;
