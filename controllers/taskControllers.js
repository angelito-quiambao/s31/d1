
const Task = require(`./../models/Task`)

// Business Logic for Creating a task
/*
1. Add a functionality to check if there are duplicate tasks
	- If the task already exists in the database, we return a message `Duplicate Task found`
	- If the task doesn't exist in the database, we add it in the database
2. The task data will be coming from the request's body
3. Create a new Task object with a "name" field/property
4. The "status" property does not need to be provided because our schema defaults it to "pending" upon creation of an object
*/
module.exports.createTask = async (reqBody) => {

	return await Task.findOne({name: reqBody.name}).then( (result, err) => {
		// console.log(result)	//a document {"name": "watching"}

		// If the task already exists in the database, we return a message `Duplicate Task found`
		if(result != null && result.name == reqBody.name){

           //`Duplicate data found`
			return true

		} else {

			// If the task doesn't exist in the database, we add it in the database

			 // Create a new Task object with a "name" field/property
			let newTask = new Task({
				name: reqBody.name
			})

			//use save() method to insert the new document in the database
			return newTask.save().then((savedTaks, err) => {
				// console.log(savedTaks)
				if(savedTaks){
					return savedTaks
				} else {
					return err
				}
			})
		}
	})

}


// Business Logic for getting all the tasks
/*
1. Retrieve all the documents
2. If an error is encountered, print the error
3. If no errors are found, send a success status back to the client/Postman and return an array of documents
*/
module.exports.getAllTasks = async () => {

	return await Task.find().then( (result, err) => {
		if(result){
			return result
		} else {
			return err
		}
	})
}

// DELETE A TASK
module.exports.deleteTask = async (id) => {
    // console.log(id)
    return await Task.findByIdAndDelete(id).then( result =>{
        
        try{
            if(result != null){
                return true
            }
            else{
                return false
            }
        }
        catch(err){
            return err
        }
    })
}

module.exports.updateTask = async (id, reqBody) =>{

  //  console.log(reqBody)
    return await Task.findByIdAndUpdate(id, {$set:reqBody}).then(result => {

        try{
            if(result){
                return result
            }
            else{
                return false
            }
        }
        catch(err){
            return err
        }
    })
}


//GET A SPECIFIC TASK

module.exports.getOneTasks = async (id) =>{
    return await Task.findById(id).then( result => {
		try{
            if(result){
                return result
            }
            else{
                return {message: `no existing document`}
            }
        }
        catch(err){
            return err
        }
	})
}


//UPDATE A SPECIFIC TASK TO CHANGE STATUS TO COMPLETE

module.exports.updateTaskStatus = async (id, reqBody) =>{
    console.log(id, reqBody)

    return await Task.findByIdAndUpdate(id, {$set: reqBody}, {new: true}).then(result => {
        try{
            if(result){
                return result
            }
            else{
                return {message: `no existing document`}
            }
        }
        catch(err){
            return err
        }
    })
}